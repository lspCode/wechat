<!DOCTYPE html >
    <head lang="zh-CN">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>绑定你的信息</title>
        <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0,maximum=1.0,user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta name="format-detection" content="telephone=yes">
        <meta name="msapplication-tap-highlight" content="no">
        <link href="css/show.css" rel="stylesheet" type="text/css">
        <meta charset="utf-8">
    </head>
    <body>
<?php
    $appid = "wx873faf7512b61145";  
    $secret = "c1efbe6e57bd6cb78ad29ecec928295c";  
    $code = $_GET["code"];

    $get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$code.'&grant_type=authorization_code';

    $ch = curl_init();  
    curl_setopt($ch,CURLOPT_URL,$get_token_url);  
    curl_setopt($ch,CURLOPT_HEADER,0);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  
    $res = curl_exec($ch);  
    curl_close($ch);  
    $json_obj = json_decode($res,true);   
    //根据openid和access_token查询用户信息  
    $access_token = $json_obj['access_token'];  
    $openid = $json_obj['openid'];  
    $get_user_info_url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';  
      
    $ch = curl_init();  
    curl_setopt($ch,CURLOPT_URL,$get_user_info_url);  
    curl_setopt($ch,CURLOPT_HEADER,0);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  
    $res = curl_exec($ch);  
    curl_close($ch); 
     //解析json  
    $user_obj = json_decode($res,true);
    
 ?>
        <div class="TopBar">
            <a href="#">
                <img id="jnu" src="http://formtest1-image.stor.sinaapp.com/%E9%A1%B5%E9%9D%A2%E5%9B%BE%E7%89%87/Simble.gif"> 
            </a>
        </div>
        <div class="top">
            <h1 class="bind">信息绑定</h1>
            <img src="http://formtest1-image.stor.sinaapp.com/%E9%A1%B5%E9%9D%A2%E5%9B%BE%E7%89%87/enrol.png" class="card">
        </div>
        <div class="body">

            <!-- <p class="page_msg">绑定你的信息</p> -->
           
            <form name="myForm" action="check.php" method="post" onsubmit="return validateForm()">
                <div class="InputDiv">
                            <span class="input_shell">
                                <input type="text" name="stname" placeholder="姓名"/>
                            </span>
               
                </div>
                <div class="InputDiv">
                    
                            <span class="input_shell">
                                <input type="text" name="idcard" placeholder="身份证号" maxlength="18"/>
                            </span>
            
                </div>
                <div class="InputDiv">
                    
                            <span class="input_shell">
                                <input type="text" name="stuid" placeholder="准考证号" maxlength="10"/>
                            </span>
               
                </div>
           
                <input type="hidden" name="wxid" value="<?php echo $openid;?>">

                <input type="submit" class="btnGreen" value="绑定"/>
                <input type="reset" class="btnGreen" value="重置">
            </form>
        </div>
        <script>
            function validateForm()
                {
                    var x=document.forms["myForm"]["stname"].value;
                    var y=document.forms["myForm"]["idcard"].value;
                    var z=document.forms["myForm"]["stuid"].value;
                if(x=="")
                {
	                alert("姓名不能为空!");
                    myForm.stname.focus();
                    return false;
                } 
               else if(y=="")
              {
	                alert("身份证不能为空!");
                    myForm.idcard.focus();
	                return false;
                }else if(z=="")
              {
	               alert("学号不能为空!");
                   myForm.stuid.focus();
	               return false;
                 }
                else
                   {
	                   document.form["myForm"].submit();
                    }
             }
</script>
    </body>
</html>
