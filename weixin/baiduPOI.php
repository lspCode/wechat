<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<script src="http://api.map.baidu.com/components?ak=j8ywPTMtTmYACru9r20R4tVb&v=1.0"></script>
	<title>检索周边</title>
	<style type="text/css">
		body, html,#allmap {width: 100%;height: 100%;overflow: hidden;margin:0;}
		#golist {display: none;}
		@media (max-device-width: 800px){#golist{display: block!important;}}
	</style>
</head>
<body>
<?php
    $appid = "wx873faf7512b61145";  
    $secret = "c1efbe6e57bd6cb78ad29ecec928295c";  
    $code = $_GET["code"];

    $get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$code.'&grant_type=authorization_code';

    $ch = curl_init();  
    curl_setopt($ch,CURLOPT_URL,$get_token_url);  
    curl_setopt($ch,CURLOPT_HEADER,0);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  
    $res = curl_exec($ch);  
    curl_close($ch);  
    $json_obj = json_decode($res,true);    
    $openid = $json_obj['openid'];
    $mem = memcache_init();
    $array = $mem->get($openid);
//$query = $array['0'];
?>
	<a id="golist" href="../demolist.htm">返回demo列表页</a>
    <lbs-nearby query="<?php print_r($array['0']);?>" radius="2000" center="<?php print_r($array['lat']);?>,<?php print_r($array['lng']);?>" type="list" style="height:100%"></lbs-nearby>
</body>
</html>