<!DOCTYPE HTML>
<head lang="zh-CN">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>个人信息</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="viewport" content="width=divice, initial-scale=1.0,minimum-scale=1.0,maximum=1.0,user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=yes">
    <meta name="msapplication-tap-highlight" content="no">
    <link href="css/show.css" rel="stylesheet" type="text/css">
    <meta charset="utf-8">
</head>
<body >
<?php
    $appid = "wx873faf7512b61145";  
    $secret = "c1efbe6e57bd6cb78ad29ecec928295c";  
    $code = $_GET["code"];  
    $get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$code.'&grant_type=authorization_code';

    $ch = curl_init();  
    curl_setopt($ch,CURLOPT_URL,$get_token_url);  
    curl_setopt($ch,CURLOPT_HEADER,0);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  
    $res = curl_exec($ch);  
    curl_close($ch);  
    $json_obj = json_decode($res,true);  
      
    //根据openid和access_token查询用户信息  
    $access_token = $json_obj['access_token'];  
    $openid = $json_obj['openid'];
    $link=mysqli_connect(SAE_MYSQL_HOST_M.':'.SAE_MYSQL_PORT,SAE_MYSQL_USER,SAE_MYSQL_PASS);
    mysqli_select_db($link,"app_formtest1");
if(empty($openid)){
    !$link;
    $student_qrcode_entry = "NULL.png";
}else{
    //$result=mysqli_query($link,"select student_name,student_sex,school_name,student_image,major,student_exam_number,student_identity_number,student_weixin_id,student_enter_date,student_qrcode_entry from weixin_students where student_weixin_id='$openid'");
  $result=mysqli_query($link,"select * from weixin_students where student_weixin_id='$openid'");
  $row=mysqli_fetch_object($result);
  $student_name = $row->student_name;
  $student_sex = $row->student_sex;
  $school_name = $row->school_name; 
  $major = $row->major;
  $student_exam_number = $row->student_exam_number;
  $student_identity_number = $row->student_identity_number;
  $student_weixin_id = $row->student_weixin_id;
  $student_enter_date= $row->student_enter_date;
  $student_qrcode_entry = $row->student_qrcode_entry;
}
//通过数据库查找对应二维码的编号选取对应的storage里面的图片
    
    $s = new SaeStorage();
    $url = $s->geturl('qrcodes',$student_qrcode_entry);
  ?>
<div class="TopBar">

    <a href="#">
        <img id="jnu" src="http://formtest1-image.stor.sinaapp.com/%E9%A1%B5%E9%9D%A2%E5%9B%BE%E7%89%87/Simble.gif">
    </a>
</div>
<div class="top">
    <h1 class="bind">我的个人信息</h1>
    
    <span class="input_shell"><img src="<?php echo $url;?>" class="erweima"></span>
    <h3 class="word">二维码</h3>
    <img src="http://formtest1-image.stor.sinaapp.com/%E9%A1%B5%E9%9D%A2%E5%9B%BE%E7%89%87/enrol.png" class="card">
    </div>
<div class="body">
    <!-- <p class="page_msg">显示你的信息</p> -->
    <form action="" method="post">
        <div class="InputDiv">
            <p align="left">姓名：</p>
                            <span class="input_shell">
                <input type="text" name="name" value="<?php echo $student_name;?>" placeholder="姓名" readonly="true"/>
            </span>
        </div>
        <div class="InputDiv">
            <p align="left">性别：</p>
                            <span class="input_shell">
                                <input type="text" name="sex" value="<?php echo $student_sex;?>" placeholder="性别" readonly="true"/>
                            </span>
        </div>
        <div class="InputDiv">
            <p align="left">学校：</p>
                            <span class="input_shell">
                                <input type="text" name="school" value="<?php echo $school_name;?>" placeholder="学校" readonly="true"/>
                            </span>
        </div>
        <div class="InputDiv">
            <p align="left">专业：</p>
                            <span class="input_shell">
                                <input type="text" name="speciality" value="<?php echo $major;?>" placeholder="专业" readonly="true"/>
                            </span>
        </div>
        <div class="InputDiv">
            <p align="left">准考证号：</p>
                            <span class="input_shell">
                                <input type="text" name="exam" value="<?php echo $student_exam_number;?>" placeholder="准考证号" readonly="true"/>
                            </span>
        </div>
        <div class="InputDiv">
            <p align="left">身份证号：</p>
                            <span class="input_shell">
                                <input type="text" name="IdCar" value="<?php echo $student_identity_number;?>" placeholder="身份证号码" readonly="true"/>
                            </span>
        </div>
        <div class="InputDiv">
            <p align="left">微信ID：</p>
                            <span class="input_shell">
                                <input type="text" name="weixinID" value="<?php echo $student_weixin_id;?>" placeholder="微信ID" readonly="true"/>
                            </span>
        </div>
        <div class="InputDiv">
            <p align="left">入学日期：</p>
                            <span class="input_shell">
                                <input type="text" name="time" value="<?php echo $student_enter_date;?>" placeholder="入学日期" readonly="true"/>
                            </span>
        </div>


    </form>
</div>
</body>
</html>