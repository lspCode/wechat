<html>
	<head>
        <meta charset="utf-8">
		<title>我的留言板</title>
	</head>
	<body>
		<center>
			<?php include("menu.php"); //导入网站的导航栏 ?>
			
			<h3>添加留言</h3>
			<?php
			//执行留言信息添加操作
			
			//1.获取要要添加的留言信息，并且补上其他辅助信息（ip地址、添加时间）
				$title = $_POST["title"];		//获取留言标题
				$author = $_POST["author"];		//获取留言者
				$content = $_POST["content"];	//留言内容
				$ip = $_SERVER["REMOTE_ADDR"];  //IP地址
				$addtime = time();				//添加时间（时间戳）
				
			//2.拼装（组装）留言信息
				$ly = "{$title}##{$author}##{$content}##{$ip}##{$addtime}@@@";
				//echo $ly;
			//3.将留言信息追加到liuyan.txt文件中 
				$info = file_get_contents("liuyan.txt");//获取所有以前的留言
				file_put_contents("liuyan.txt",$info.$ly);
			//4.输出留言成功！
				echo "留言成功！谢谢！";
			?>
		</center>
	</body>
</html>