<?php
//微信墙配置文件
define('SITE_NAME','微信墙');//微信墙名称
define('COPY_INFO','武纺新生助手');//微信墙版权
define('LOWCSS',FALSE);//是否让低端手机（安卓4.0及以下）也显示背景图。低端机浏览器性能低，不建议开启
define('IP_ACCESS_INTERVAL', 5);//重复发布帖子最小间隔，防止刷帖
define('CLASS_1','心愿');//栏目1
define('CLASS_2','树洞');//栏目2
define('CLASS_3','表白');//栏目3
define('DATABASE',SAE_MYSQL_DB);//数据库
define('HOST',SAE_MYSQL_HOST_M);//主库
define('HOST_S',SAE_MYSQL_HOST_S);//从库
define('PORT',SAE_MYSQL_PORT);//端口
define('USER',SAE_MYSQL_USER);//用户名
define('PASSWORD',SAE_MYSQL_PASS);//密码
define('WALL','wechat_wall_content');//微信说说数据表
define('WALL_COMMENT','wechat_wall_comment');//微信评论数据表
?>