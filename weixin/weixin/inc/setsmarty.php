<?php
//设置模板
require('smarty/Smarty.class.php');
class smarty_pzblog extends Smarty
 {
 	function __construct()
 	{
 		parent::__construct();
        $this->setCompileDir('saemc://pzblog/');//设置编译目录
        $this->setConfigDir('../');//设置配置文件目录
        $this->debugging = false;//调试
        $this->caching = false;//是否启用缓存，项目在调试过程中不建议启用缓存
        $this->cache_lifetime = 120;//定义模板缓存文件的有效时间
        $this->compile_locking = false; //编译锁
   }
}
?>