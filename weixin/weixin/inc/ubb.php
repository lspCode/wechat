<?php
function badwordxx($str,$badwords)
{
  $bstr = file_get_contents(dirname(__FILE__).'/badwords.php');//获得敏感词的内容
  $badwords = array_diff(explode("\n",$bstr),array(null));//将字符串打散为数组后与空数组的差集
  $sta = '*';//敏感词替换成*
  $badword1 = array_combine($badwords,array_fill(0,count($badwords),$sta));//得到一个新数组将所有的敏感字设置为*
  return strtr($str, $badword1);//将输入的敏感字替换为*
}
function straddubb($str)
{//将所有的html标识符替换为*
  $str = badwordxx($str,$badwords); 
  $str=str_replace('[p]','<p>',$str);
  $str=str_replace('[/p]','</p>',$str);
  $str=str_replace('[b]','<b>',$str);
  $str=str_replace('[/b]','</b>',$str);
  $str=str_replace('[hr]','<hr />',$str);
  $str=str_replace('  ','&nbsp;&nbsp;',$str);
  $str=preg_replace('|\[img=(.*?)\]|ism','<img src="$1" />',$str);
  $str=preg_replace('|\[a=(.*?)\](.*?)\[/a\]|ism','<a href="$1">$2</a>',$str);
  $str=preg_replace('|\[url=(.*?)\]|ism','<a href="$1">$1</a>',$str);
  $str=str_replace('(*;)','[',$str);
  $str=str_replace('(#;)',']',$str);
  return $str;
}
