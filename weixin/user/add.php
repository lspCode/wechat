<html>
<head>
      <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
      <title>用户信息管理</title>
      <style type="text/css">
      .center{text-align:center;}
      </style>
</head>
<body>
<center>
      <?php include 'menu.php';//导入导航栏?>
      <h3>浏览用户信息</h3>
      <form action="action.php?a=add" method="post">
      <table width="300" border="0" >
      <tr>
         <td>学号：</td>
         <td><input type="text" name="student_code"/></td>
      </tr>
      <tr>
         <td>姓名：</td>
         <td><input type="text" name="student_name"/></td>
      </tr>
      <tr>
         <td>学校名称：</td>
         <td><input type="text" name="school_name"/></td>
      </tr>
      <tr>
         <td>学院ID：</td>
         <td><input type="text" name="academy_id" placeholder="1~3选一个"/></td>
      </tr>
      <tr>
         <td>专业：</td>
         <td><input type="text" name="major" placeholder="目前只有三个学院"/></td>
      </tr>
      <tr>
         <td>性别：</td>
         <td><input type="radio" name="student_sex" value="女" />女
             <input type="radio" name="student_sex" value="男" />男</td>
      </tr>
      <tr>
         <td>身份证号：</td>
         <td><input type="text" name="student_identity_number" placeholder="随便填个18位"/></td>
      </tr>
      <tr>
         <td>准考证号：</td>
         <td><input type="text" name="student_exam_number" placeholder="随便写一个10位数记得绑定需要"/></td>
      </tr>
      <tr>
         <td>电话号码：</td>
         <td><input type="text" name="student_phone_number" value="0"/></td>
      </tr>
      <tr>
         <td>入学日期：</td>
         <td><input type="date" name="student_enter_date"/></td>
      </tr>
      <tr>
         <td>&nbsp</td>
         <td><input type="submit" value="添加" />
             <input type="reset" value="重置" /></td>
      </tr>
      </table>
      </form>
</center>
</body>
</html>