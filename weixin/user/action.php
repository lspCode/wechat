<?php 
//执行信息的增删改等操作的action
header("Content-Type:text/html;charset=utf-8");
//1，导入配置文件
require 'dbconfig.php';

//2.连接数据库，并选择数据库
$link = @mysql_connect(HOST,USER,PASS) or die("数据库连接失败！");
//mysql_query("set names utf-8");
mysql_select_db(DBNAME,$link);

//3，根据动作参数a值，做对应的增，删，改操作
switch ($_GET['a']){
	case "add":  //执行添加
	    //获取要添加的信息
	    $student_code = $_POST['student_code'];
	    $student_name = $_POST['student_name'];
	    $school_name = $_POST['school_name'];
	    $academy_id = $_POST['academy_id'];
	    $major = $_POST['major'];
	    $student_sex = $_POST['student_sex'];
	    $student_identity_number = $_POST['student_identity_number'];
	    $student_exam_number = $_POST['student_exam_number'];
	    $student_phone_number = $_POST['student_phone_number'];
	    $student_enter_date = $_POST['student_enter_date'];
    //mysql_query("set names utf-8");
		//拼装添加sql语句
    $sql = "insert into weixin_students (student_code,student_name,school_name,academy_id,major,student_sex,student_identity_number,student_exam_number,student_phone_number,student_enter_date)values({$student_code},'{$student_name}','{$school_name}',{$academy_id},'{$major}','{$student_sex}',{$student_identity_number},{$student_exam_number},{$student_phone_number},'{$student_enter_date}')"; 
		//执行添加操作
		mysql_query($sql,$link);
		//判断是否成功
		if (mysql_affected_rows($link)>0){
			echo "<h3>添加成功!</h3>";
		}else {
			echo "<h3>添加失败!</h3>";
		}
		break;
		
    case "del":  //执行删除
		//获取要删除的id号
		$student_id = $_GET['student_id'];
    	//执行删除
    	mysql_query("delete from weixin_students where student_id=".$student_id);
    	//跳回查询信息页面
    	header("location:index.php");
    	exit();
		break;
			
	case "update":  //执行修改
		//获取要修改的信息
		$student_code = $_POST['student_code'];
	    $student_name = $_POST['student_name'];
	    $school_name = $_POST['school_name'];
	    $academy_id = $_POST['academy_id'];
	    $major = $_POST['major'];
	    $student_sex = $_POST['student_sex'];
	    $student_identity_number = $_POST['student_identity_number'];
	    $student_exam_number = $_POST['student_exam_number'];
	    $student_phone_number = $_POST['student_phone_number'];
	    $student_weixin_id = $_POST['student_weixin_id'];
	    $student_qrcode_entry = $_POST['student_qrcode_entry'];
	    $student_enter_date = $_POST['student_enter_date'];
        $student_id = $_POST['student_id'];
		//拼装修改sql语句
    $sql = "update weixin_students set student_code={$student_code},student_name='{$student_name}',school_name='{$school_name}',academy_id={$academy_id},major='{$major}',student_sex='{$student_sex}',student_identity_number={$student_identity_number},student_exam_number={$student_exam_number},student_phone_number={$student_phone_number},student_enter_date='{$student_enter_date}' where student_id={$student_id}";
		//执行修改操作
		mysql_query($sql,$link);
		//判断是否成功
		if (mysql_affected_rows($link)>0){
		echo "<h3>修改成功!</h3>";
		}else {
		echo "<h3>修改失败!</h3>";
		}
		break;
}

//4，关闭数据库连接
mysql_close($link);

echo "<a href='javascript:window.history.back();'>返回</a> ";
echo "<a href='index.php'>浏览用户信息</a>";
?>