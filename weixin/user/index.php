<html>
<head>
      <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
      <title>微信绑定用户信息管理</title>
      <script type="text/javascript">
      function doDel(student_id){
          if(confirm("确定要删除吗？")){
              window.location="action.php?a=del&student_id="+student_id;
              }
          }
      </script>
</head>
<body>
<center>
<?php include 'menu.php';?>
<h3>浏览用户信息</h3>
      <table width="1300" border="1">
      <tr>
          <th>学号</th>
          <th>姓名</th>
          <th>学校名称</th>
          <th>学院ID</th>
          <th>专业</th>
          <th>性别</th>
          <th>身份证号</th>
          <th>准考证号</th>
          <th>电话号码</th>
          <th>微信ID</th>
          <th>二维码</th>
          <th>入学日期</th>
          <th>操作</th>
      </tr>
      <?php 
      require 'dbconfig.php';
      $link = @mysql_connect(HOST,USER,PASS)or die("数据库连接失败！");
      mysql_select_db(DBNAME,$link);
      //2.1 插入分页处理代码
      //======================================
      //1. 定义一些分页变量
      $page=isset($_GET["page"])?$_GET['page']:1;		//当前页号
      $pageSize=3;	//页大小
      $maxRows;		//最大数据条
      $maxPages;		//最大页数
      
      //2. 获取最大数据条数
      $sql = "select count(*) from weixin_students";
      $res = mysql_query($sql,$link);
      $maxRows = mysql_result($res,0,0); //定位从结果集中获取总数据条数这个值。
      //3. 计算出共计最大页数
      $maxPages = ceil($maxRows/$pageSize); //采用进一求整法算出最大页数
      
      //4. 效验当前页数
      if($page>$maxPages){
      	$page=$maxPages;
      }
      if($page<1){
      	$page=1;
      }
      
      //5. 拼装分页sql语句片段
      $limit = " limit ".(($page-1)*$pageSize).",{$pageSize}";   //起始位置是当前页减一乘以页大小
      //======================================
      $sql = "select *from weixin_students order by student_id desc {$limit}";
      $result = mysql_query($sql,$link);
      while ($row = mysql_fetch_assoc($result))
      {
      	echo "<tr>";
      	echo "<td>{$row['student_code']}</td>";
      	echo "<td>{$row['student_name']}</td>";
      	echo "<td>{$row['school_name']}</td>";
      	echo "<td>{$row['academy_id']}</td>";
      	echo "<td>{$row['major']}</td>";
      	echo "<td>{$row['student_sex']}</td>";
      	echo "<td>{$row['student_identity_number']}</td>";
      	echo "<td>{$row['student_exam_number']}</td>";
      	echo "<td>{$row['student_phone_number']}</td>";
      	echo "<td>{$row['student_weixin_id']}</td>";
      	echo "<td>{$row['student_qrcode_entry']}</td>";
      	echo "<td>{$row['student_enter_date']}</td>";
      	echo "<td>
      	<a href='javascript:doDel({$row['student_id']})'>删除</a>
      			<a href='edit.php?student_id={$row['student_id']}'>修改</a></td>";
      	echo "</tr>";
      }
      ?>
      </table>
      <?php
					//输出分页信息，显示上一页和下一页的连接
					echo "<br/><br/>";
					echo "当前{$page}/{$maxPages}页 共计{$maxRows}条";
					echo " <a href='index.php?page=1'>首页</a> ";
					echo " <a href='index.php?page=".($page-1)."'>上一页</a> ";
					echo " <a href='index.php?page=".($page+1)."'>下一页</a> ";
					echo " <a href='index.php?page={$maxPages}'>末页</a> ";
				?>
</center>
</body>
</html>